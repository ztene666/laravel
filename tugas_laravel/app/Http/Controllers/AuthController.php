<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('halaman.register');
    }

    public function kirim(Request $request){
        $fnama = $request['fname'] ;
        $lnama = $request['lname'];
        $gender = $request['gender'];
        $wn = $request ['wn'];
        return view('halaman.home', compact('fnama','lnama'));
    }
}
